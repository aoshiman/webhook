# -*- coding: utf-8 -*-
import socket

# product name
PRODUCT = "flasklog"

# your repostory folder
DIR = "/home/www-data/public_html/" + PRODUCT

# bitbucket ip
# REMOTE_IP = ["207.223.240.187", "207.223.240.188", "63.246.22.222"]

# bitbucket user_agent
USER_AGENT = "Bitbucket.org"

#production hostname
HOSTNAME = 'www15133uj'

#deboug mode
DEBUG_MODE = False
if socket.gethostname() != HOSTNAME:
    DEBUG_MODE = True
