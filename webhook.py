# -*- coding: utf-8 -*-

import os
from time import sleep
from subprocess import call
from bottle import (route, post, get, error,
                    request, run, redirect, default_app)
from setting import DIR, USER_AGENT, DEBUG_MODE

user_agent = USER_AGENT
debug = DEBUG_MODE

@get("/hook")
def get_page():
    redirect("/")


@post("/hook")
def web_hook():
    if debug:
        return "DEBUG MODE POST OK"
    elif user_agent == request.environ.get("HTTP_USER_AGENT"):
        os.chdir(DIR)
        call("/usr/bin/git --git-dir=.git pull", shell=True)
        sleep(1)
        call("/bin/rm -rf /var/cache/nginx/flasklog/*", shell=True)
        return "WEBHOOK OK"


@route("/")
def hello():
    req_ip = request.remote_addr
    return "Hello " + req_ip


@error(404)
def error404(error):
    return 'Nothing here, sorry'


if __name__ == '__main__':
    run()
else:
    application = default_app()
